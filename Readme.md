BR.FRITZEN.ENGINE
=================

This game/render engine has only study purposes.

I'm sharing the code to help newbies (like me) to understand better the usage of modern OpenGL
with shaders and the basic hierarchical structure needed to everything work fine.


Setup
-----

Import to your preferred IDE as a Maven Project.



Running
-------

Try run the examples presented in src/test/java/ they are useful for newbies in game programing.

