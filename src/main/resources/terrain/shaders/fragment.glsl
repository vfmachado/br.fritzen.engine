#version 430

layout (location = 0) out vec4 outputColor;

in vec2 mapCoord_FS;
in float positiony;

uniform sampler2D normalmap;

const vec3 lightDirection = vec3(0.1, -1.0, 0.1);
const float lightIntensity = 1.2;

const vec3 mountain = vec3(203.0, 206.0, 224.0)/255;
const vec3 sand = vec3(76.0/255, 70.0/255, 50.0/255);


float calcDiffuse(vec3 direction, vec3 normal, float intensity) {

	return max(0.01, dot(normal, -direction) * intensity);

}


void main() {

	vec3 normal = texture(normalmap, mapCoord_FS).rgb;
	
	float diff = calcDiffuse(lightDirection, normal, lightIntensity);
	
	//vec3 rgb = normal * diff;
	
	float fadeFactor = 1.0 - smoothstep(200.0, 600.0, positiony);
	vec3 rgb = mix(mountain, sand, fadeFactor) * diff;
	
	outputColor = vec4(rgb, 1);

}