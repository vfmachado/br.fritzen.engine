#version 430

in vec3 color0;

out vec4 fragColor;

void main() {

	fragColor = vec4(color0, 1.0);

}
