#version 430

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 color0;

void main() {

	mat4 MVP = projection * view * model;
	
	gl_Position = MVP * vec4(position.xyz, 1.0f);
	
	color0 = color;
}