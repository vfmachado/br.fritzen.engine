#version 430

in vec3 worldPosition;

vec3 baseColor = vec3(0.18, 0.27, 0.47);

uniform float scale;

const vec4 colour1 = vec4(0.835, 1.0, 1.0, 1.0);
const vec4 colour2 = vec4(0.164, 0.254, 0.458, 1.0);

out vec4 fragColor;

void main() {


	//float red = -0.00022*(abs(worldPosition.y)-3000) + baseColor.x;
	//float green = -0.00025*(abs(worldPosition.y)-3000) + baseColor.y;
	//float blue = -0.00019*(abs(worldPosition.y)-3000) + baseColor.z;
	//fragColor = vec4(red, green, blue, 1);

	float fadeFactor = 1.0 - smoothstep(0.0 - worldPosition.y, scale*0.65, worldPosition.y);
	fragColor = mix(colour2, colour1, fadeFactor);
	
		

}
