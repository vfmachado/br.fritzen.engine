#version 430

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out vec3 worldPosition;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main() {

	mat4 MVP = projection * view * model;
	gl_Position = MVP * vec4(position.xyz, 1.0f);
	
	worldPosition = (model * vec4(position, 1.0f)).xyz;
	
}