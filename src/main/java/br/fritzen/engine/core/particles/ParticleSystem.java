package br.fritzen.engine.core.particles;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameComponent;

public class ParticleSystem extends GameComponent {

	public int blendMode = GL11.GL_ONE;
	
	private ParticleShader shader;

	private ArrayList<Particle> particles;

	private float pps;

	private float speed;

	private float gravityComplient;

	private float lifeLength;

	private float speedError;

	private float lifeError;

	private float scale;

	private float scaleError;
	
	private Vector3f direction;

	private float directionDeviation;
	
	private ParticleTexture texture;
	
	private int pointer = 0;
	
	private float vboData[];// = new float[50_000 * ParticleShader.INSTANCE_DATA_LENGTH];;
	
	private float radiusError;
	
	
	public ParticleSystem(ParticleTexture particleTexture, float particlesPerSecond, float speed, float gravityComplient, float lifeLength) {

		this.shader = ParticleShader.getInstance();
		this.particles = new ArrayList<Particle>();

		this.texture = particleTexture;
		
		this.pps = particlesPerSecond;
		this.speed = speed;
		this.gravityComplient = gravityComplient;
		this.lifeLength = lifeLength;

		this.speedError = 0;
		this.lifeError = 0;
		this.scaleError = 0;
		this.directionDeviation = 0;
		this.scale = 1;
		
		this.radiusError = 0;
		
		//vboData = new float[(int)(particlesPerSecond * lifeLength/100 + 1) * ParticleShader.INSTANCE_DATA_LENGTH];
		vboData = new float[50000 * ParticleShader.INSTANCE_DATA_LENGTH];
	}

	
	public void addParticle(Particle particle) {
		this.particles.add(particle);
	}

	
	@Override
	public void update() {

		if (particles.size() < pps * lifeLength/100) {
			
			float particlesToCreate = pps  / GS.UPS;
			int count = (int) Math.floor(particlesToCreate);
			float partialParticle = particlesToCreate % 1;
	
			for (int i = 0; i < count; i++) {
				particles.add(addParticle(this.getParent().getTransform().getTransformedPosition(), null));
			}
	
			if (Math.random() < partialParticle) {
				particles.add(addParticle(this.getParent().getTransform().getTransformedPosition(), null));
			}
		}
		
		
		for (int i = 0; i < particles.size(); i++) {
			
			if (!particles.get(i).update()) {
				if (particles.size() < pps * lifeLength/100) {
					addParticle(this.getParent().getTransform().getTransformedPosition(), particles.get(i));
				} else {
					particles.remove(i);
					i--;
				}
			}
			
		}

		pointer = 0;
		
		try {
			for (Particle p : particles) {
				storeMatrixData(p.updateModelViewMatrix(), vboData);
				updateTexCoordInfo(p, vboData);
			}
		} catch (Exception e) {
			System.out.println("MAX OF ALLOWED PARTICLES REACHED... PLS REVIEW YOUR CODE");
		}
		
		this.shader.bind();
		GL30.glBindVertexArray(shader.getParticleVao());
		shader.updateVBO(vboData);
	}

	
	@Override
	public void renderOnce() {

		this.shader.bind();
		GL30.glBindVertexArray(shader.getParticleVao());
		
		shader.updateUniform("projectionMatrix", Game.getScene().getCamera().getProjectionBuffer());
		shader.updateUniform("numberOfRows", (float)texture.getNumberOfRows());
		this.texture.getTexture().bind(0);
				
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, blendMode);
		GL11.glDepthMask(false);
		
		GL31.glDrawArraysInstanced(GL11.GL_TRIANGLE_STRIP, 0, 4, particles.size());
		
		GL11.glDepthMask(true);
		GL11.glDisable(GL11.GL_BLEND);

		GL30.glBindVertexArray(0);
		this.shader.unbind();
	}

	
	private void storeMatrixData(Matrix4f matrix, float[] vboData) {

		vboData[pointer++] = matrix.m00();
		vboData[pointer++] = matrix.m01();
		vboData[pointer++] = matrix.m02();
		vboData[pointer++] = matrix.m03();
		vboData[pointer++] = matrix.m10();
		vboData[pointer++] = matrix.m11();
		vboData[pointer++] = matrix.m12();
		vboData[pointer++] = matrix.m13();
		vboData[pointer++] = matrix.m20();
		vboData[pointer++] = matrix.m21();
		vboData[pointer++] = matrix.m22();
		vboData[pointer++] = matrix.m23();
		vboData[pointer++] = matrix.m30();
		vboData[pointer++] = matrix.m31();
		vboData[pointer++] = matrix.m32();
		vboData[pointer++] = matrix.m33();

	}
	
	
	private void updateTexCoordInfo(Particle particle, float[] data) {
		
		data[pointer++]  = particle.getTexOffset1().x;
		data[pointer++]  = particle.getTexOffset1().y;
		data[pointer++]  = particle.getTexOffset2().x;
		data[pointer++]  = particle.getTexOffset2().y;
		data[pointer++]  = particle.getTexBlend();
		
	}
	
	
	@Override
	public String getComponentName() {
		return ParticleSystem.class.getSimpleName();
	}

	
	private Vector3f velocity = new Vector3f();
	private Particle addParticle(Vector3f center, Particle particle) {

		if (direction != null) {
            generateRandomUnitVectorWithinCone(direction, directionDeviation, velocity);
        } else {
            velocity = generateRandomUnitVector();
        }
		
		velocity.normalize();
		velocity.mul(speed);

		float scale = getRandomValue(this.scale, this.scaleError);
		float lifeLength = getRandomValue(this.lifeLength, lifeError);
		
		if (particle == null) {
			
			return new Particle(this.texture, new Vector3f(center).add(getRandomValue(0, radiusError), 0, getRandomValue(0, radiusError)), new Vector3f(velocity), gravityComplient, lifeLength, (float)(Math.random() * 360f), scale);
			
		} else {
			
			particle.getPosition().set(center).add(getRandomValue(0, radiusError), 0, getRandomValue(0, radiusError));
			particle.getVelocity().set(velocity);
			particle.setElapsedTime(0);
			particle.setLifeLength(lifeLength);
			particle.setRotation((float)(Math.random() * 360f));
			particle.setScale(scale);
			
			return particle;
		}
	}

	
	static Vector3f rotateAxis = new Vector3f();
	private static Vector3f generateRandomUnitVectorWithinCone(Vector3f coneDirection, float angle, Vector3f dest) {
		
		float cosAngle = (float) Math.cos(angle);
		float theta = (float) (Math.random() * 2f * Math.PI);
		float z = (float) (cosAngle + (Math.random() * (1 - cosAngle)));
		float rootOneMinusZSquared = (float) Math.sqrt(1 - z * z);
		float x = (float) (rootOneMinusZSquared * Math.cos(theta));
		float y = (float) (rootOneMinusZSquared * Math.sin(theta));

		//Vector3f direction = new Vector3f(x, y, z);
		dest.set(x, y, z);
		if (coneDirection.x != 0 || coneDirection.y != 0 || (coneDirection.z != 1 && coneDirection.z != -1)) {
			rotateAxis.set(coneDirection).cross(GS.Z_AXIS);
			rotateAxis.normalize();
			float rotateAngle = (float) Math.acos(coneDirection.dot(GS.Z_AXIS));
			Matrix4f rotationMatrix = new Matrix4f();
			rotationMatrix.rotate(-rotateAngle, rotateAxis);
			dest.mulPositionW(rotationMatrix);
			
		} else if (coneDirection.z == -1) {
			dest.z *= -1;
		}
		
		return dest;
	}

	
	private Vector3f generateRandomUnitVector() {
		float theta = (float) (Math.random() * 2f * Math.PI);
		float z = (float) ((Math.random() * 2) - 1);
		float rootOneMinusZSquared = (float) Math.sqrt(1 - z * z);
		float x = (float) (rootOneMinusZSquared * Math.cos(theta));
		float y = (float) (rootOneMinusZSquared * Math.sin(theta));
		return new Vector3f(x, y, z);
	}
	
	
	private float getRandomValue(float baseValue, float errorMargin) {
		float offset = (float) (Math.random() - 0.5f) * 2f * errorMargin;
		return baseValue + offset;
	}

	
	public float getSpeedError() {
		return speedError;
	}

	
	public void setSpeedError(float speedError) {
		this.speedError = speedError;
	}

	
	public float getLifeError() {
		return lifeError;
	}

	
	public void setLifeError(float lifeError) {
		this.lifeError = lifeError;
	}

	
	public float getScaleError() {
		return scaleError;
	}

	
	public void setScaleError(float scaleError) {
		this.scaleError = scaleError;
	}

	
	public float getScale() {
		return scale;
	}

	
	public void setScale(float scale) {
		this.scale = scale;
	}
	
	
	public void setDirection(Vector3f direction) {
		this.direction = direction;
	}


	public void setDirectionDeviation(float directionDeviation) {
		this.directionDeviation = directionDeviation;
	}


	public void setTexture(ParticleTexture texture) {
		this.texture = texture;
	}


	public void setPps(float pps) {
		this.pps = pps;
	}


	public void setSpeed(float speed) {
		this.speed = speed;
	}


	public void setGravityComplient(float gravityComplient) {
		this.gravityComplient = gravityComplient;
	}


	public void setLifeLength(float lifeLength) {
		this.lifeLength = lifeLength;
	}


	public Vector3f getDirection() {
		return this.direction;
	}
	
	
	public void setRadiusError(float radiusError) {
		this.radiusError = radiusError;
	}
}
