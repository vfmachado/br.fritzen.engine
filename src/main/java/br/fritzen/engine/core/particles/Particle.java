package br.fritzen.engine.core.particles;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.utils.MyMath;

public class Particle {

	private static float GRAVITY = -1f;
	
	private Matrix4f modelMatrix;
	private Matrix4f modelViewMatrix;
	private Matrix4f viewMatrix;
	
	private Vector3f position;
	private Vector3f velocity;
	private float gravityEffect;
	private float lifeLength;
	private float rotation;
	private float scale;
	
	private float elapsedTime = 0;
	
	private ParticleTexture texture;
	
	private Vector2f texOffset1 = new Vector2f();
	private Vector2f texOffset2 = new Vector2f();
	private float texBlend;
	
	public Particle(ParticleTexture particleTexture, Vector3f position, Vector3f velocity, float gravityEffect, float lifeLength, float rotation,
			float scale) {
		
		this.texture = particleTexture;
		this.position = position;
		this.velocity = velocity;
		this.gravityEffect = gravityEffect;
		this.lifeLength = lifeLength;
		this.rotation = rotation;
		this.scale = scale;
		
		this.modelMatrix = new Matrix4f();
		this.modelViewMatrix = new Matrix4f();
		
	}
	
	
	public boolean update() {
	
		velocity.y += GRAVITY * gravityEffect;
		position.add(velocity);
		
		elapsedTime++;
		
		updateTextCoords();
		
		return elapsedTime < lifeLength;
	}
	
	
	private void updateTextCoords() {
		
		float lifeFactor = elapsedTime / lifeLength;
		int stages = texture.getNumberOfRows() * texture.getNumberOfRows();
		float atlasProgression = lifeFactor * stages;
		int index1 = (int) Math.floor(atlasProgression);
		int index2 = index1 < stages -1  ? index1 + 1 : index1;
		this.texBlend = atlasProgression % 1;
		this.setTextOffset(texOffset1, index1);
		this.setTextOffset(texOffset2, index2);
		
	}
	
	
	private void setTextOffset(Vector2f offset, int index) {
		
		int column = index % texture.getNumberOfRows();
		int row = index / texture.getNumberOfRows();
		
		offset.x = (float) column / texture.getNumberOfRows();
		offset.y = (float) row / texture.getNumberOfRows();
		
	}
	
	
	public Vector3f getPosition() {
		return position;
	}


	public float getRotation() {
		return rotation;
	}


	public float getScale() {
		return scale;
	}


	public Matrix4f updateModelViewMatrix() {
		
		viewMatrix = Game.getScene().getCamera().getView();
		
		modelMatrix.identity();
		modelMatrix.translate(position);
		
		modelMatrix.m00(viewMatrix.m00());
	    modelMatrix.m01(viewMatrix.m10());
	    modelMatrix.m02(viewMatrix.m20());
	    modelMatrix.m10(viewMatrix.m01());
	    modelMatrix.m11(viewMatrix.m11());
	    modelMatrix.m12(viewMatrix.m21());
	    modelMatrix.m20(viewMatrix.m02());
	    modelMatrix.m21(viewMatrix.m12());
	    modelMatrix.m22(viewMatrix.m22());
	    
	    modelMatrix.rotate(MyMath.toRadians(this.getRotation()), GS.Z_AXIS);
	    modelMatrix.scale(this.getScale());
	    
	    modelViewMatrix.identity();
	    viewMatrix.mul(modelMatrix, modelViewMatrix);
		
	    return modelViewMatrix;
	}


	public Matrix4f getModelViewMatrix() {
		return modelViewMatrix;
	}


	public ParticleTexture getTexture() {
		return texture;
	}


	public Vector2f getTexOffset1() {
		return texOffset1;
	}


	public Vector2f getTexOffset2() {
		return texOffset2;
	}


	public float getTexBlend() {
		return texBlend;
	}


	public Vector3f getVelocity() {
		return velocity;
	}


	public void setVelocity(Vector3f velocity) {
		this.velocity = velocity;
	}


	public float getLifeLength() {
		return lifeLength;
	}


	public void setLifeLength(float lifeLength) {
		this.lifeLength = lifeLength;
	}


	public float getElapsedTime() {
		return elapsedTime;
	}


	public void setElapsedTime(float elapsedTime) {
		this.elapsedTime = elapsedTime;
	}


	public void setPosition(Vector3f position) {
		this.position = position;
	}


	public void setRotation(float rotation) {
		this.rotation = rotation;
	}


	public void setScale(float scale) {
		this.scale = scale;
	}

	
	

}
