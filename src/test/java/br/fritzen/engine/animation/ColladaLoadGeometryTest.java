package br.fritzen.engine.animation;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.formats.collada.ColladaLoader;
import br.fritzen.engine.formats.collada.ColladaModel;
import br.fritzen.engine.formats.collada.libraries.ColladaGeometry;
import br.fritzen.engine.model.IndexedModel;
import br.fritzen.engine.utils.MyMath;


public class ColladaLoadGeometryTest extends Game {

	private float timer = 0;

	private float Z = 0.0f;
	
	private Camera camera;
	
	private GameObject gameObject;	
	
	public static void main(String[] args) {
		new ColladaLoadGeometryTest(1366, 768, "TESTE").start();
	}

	
	public ColladaLoadGeometryTest(int width, int height, String title) {
		super(width, height, title);
	}

		
	@Override
	protected void init() {
		
		Scene scene = Game.getScene();
		
		scene.setAmbientLight(new Vector3f(0.8f, 0.8f, 0.8f));
		scene.setSunLight(new SunLight(new Vector3f(1, 1, 1), 1.0f, new Vector3f(-0.6f, -0.8f, -0.6f)));
		
		camera = new Camera((float) Math.toRadians(70f), 1366/768f, 0.01f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 10, 15));
		camera.addGameComponent(new FreeMove(this.getInput(), 0.2f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.2f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		scene.setCamera(camera);
		
		gameObject = new GameObject();
		
		ColladaLoader loader = new ColladaLoader();
		ColladaModel model = loader.load("src/main/resources/collada/model.dae");
		
		Material material = new Material().createBlank();
		material.setTexture(new Texture("src/main/resources/collada/model.png"));
		//material.setDiffuseColor(new Vector3f(1f));
		
		for (ColladaGeometry geometry : model.getLibraryGeometries()) {
			IndexedModel indexedModel0 = new IndexedModel(
					geometry.getVertices(),
					geometry.getTexCoords(),
					geometry.getNormals(),
					geometry.getIndicesIndexedModel()
					); 
			Mesh mesh0 = new Mesh(indexedModel0);
			gameObject.addGameComponent(new MeshRenderer(mesh0, material));
		}
		
		//gameObject.getTransform().setScale(new Vector3f(0.1f));
		gameObject.getTransform().rotate(new Vector3f(1, 0, 0), MyMath.toRadians(-90));
		scene.addGameObject(gameObject);
		
	}

	float rotationSpeed = 0.5f;
	@Override
	protected void update() {
		super.update();
		
		gameObject.getTransform().rotate(new Vector3f(0, 1, 0), MyMath.toRadians(rotationSpeed));
		
	}
	
	
}
