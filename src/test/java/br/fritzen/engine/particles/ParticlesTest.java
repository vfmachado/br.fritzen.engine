package br.fritzen.engine.particles;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.core.particles.ParticleSystem;
import br.fritzen.engine.core.particles.ParticleTexture;

public class ParticlesTest extends Game {

	Texture tex = new Texture("src/main/resources/particles/snowflake.png");
	ParticleSystem particleSystem = new ParticleSystem(new ParticleTexture(tex, 1), 100, 0.5f, 0.002f, 500);
	
	public ParticlesTest() {
		super(1280, 720, "Particles Test");
	}

	public static void main(String[] args) {
		new ParticlesTest().start();
	}

	@Override
	protected void init() {
		
		this.setShadowMap(false);
		this.setAmbientColor(new Vector4f(0.2f));
		
		Scene scene = Game.getScene();
		
		scene.setAmbientLight(new Vector3f(0.2f));
		SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1, new Vector3f(0.5f, 1f, 0.5f));
		scene.setSunLight(sun);
		
		Camera camera = new Camera((float) Math.toRadians(70f), 720f/480f, 0.01f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 30));
		camera.addGameComponent(new FreeMove(this.getInput(), 0.5f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.3f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		//TEST 1
		Mesh mesh1 = new Mesh("src/main/resources/models/plane/plane.obj");
		Material material1 = new Material();
		material1.setTexture(new Texture("src/main/resources/images/defaultTexture.png"));
		material1.setSpecularExponent(1);
		GameObject floor = new GameObject("obj1");
		floor.addGameComponent(new MeshRenderer(mesh1, material1));
		floor.getTransform().setScale(new Vector3f(50, 1, 50));
		floor.getTransform().setPosition(new Vector3f(0, -10, 0));
		scene.addGameObject(floor);
		
		
		GameObject particleEmitter = new GameObject();
		scene.addGameObject(particleEmitter);
		particleEmitter.getTransform().getPosition().z = 5;
		particleEmitter.addGameComponent(particleSystem);
		
		//criar variavel do tipo GameObject
		//GameObject dragon = new GameObject();
		//adicionar a variavel na cena
		//scene.addGameObject(dragon);
		//dragon.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/dragon/model.obj")));
		//dragon.getTransform().getPosition().y = -7;
		//dragon.getTransform().rotate(GS.Y_AXIS, MyMath.toRadians(90f));
		
		
		particleSystem.setLifeError(0.5f);
		particleSystem.setSpeedError(0.2f);
		//particleSystem.setScale(10f);
		particleSystem.setScaleError(0.8f);
		particleSystem.setDirection(new Vector3f(0, 1, 0));
		particleSystem.setDirectionDeviation(0.4f);
		
		
	}

	@Override
	public void input() {
		super.input();
		
		
	}
	
}
