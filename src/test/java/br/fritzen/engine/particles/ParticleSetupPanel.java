package br.fritzen.engine.particles;

import org.joml.Quaternionf;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import br.fritzen.engine.gui.GUIElement;
import br.fritzen.engine.gui.elements.Button;
import br.fritzen.engine.gui.elements.Label;
import br.fritzen.engine.gui.elements.Panel;
import br.fritzen.engine.gui.elements.Slider;
import br.fritzen.engine.utils.MyMath;

public class ParticleSetupPanel extends GUIElement {

	Panel backGroundPanel;
	Label genericLabel;
	
	Slider xDirSlider;
	Slider yDirSlider;
	Slider zDirSlider;
	
	Slider speed;
	Slider pps;
	Slider gravityComp;
	
	Slider life;
	Slider size;
	Slider deviation;
	
	Button btnDefault;
	
	Vector3f position;
	Vector3f scale;
	Quaternionf rotation;
	
	Slider radiusError;
	
	public ParticleSetupPanel() {
	
		position = new Vector3f();
		scale = new Vector3f(1);
		rotation = new Quaternionf();
		
		backGroundPanel = new Panel(new Vector4f(0, 0, 1, 1), new Vector4f(0.3f, 0.3f, 0.3f, 0.8f));
		backGroundPanel.setRoundedRadius(5f);
		this.add(backGroundPanel);
	
		btnDefault = new Button(new Vector4f(0.7f, 0.005f, 0.29f, 0.05f), new Vector4f(0.2f, 0.2f, 0.2f, 1.0f), new Vector4f(1), new Vector4f(0.4f)) {
			
			@Override
			protected void clickAction() {
				
				xDirSlider.setValue(0);
				yDirSlider.setValue(1);
				zDirSlider.setValue(0);
				
				speed.setValue(0.4f);
				pps.setValue(100);
				gravityComp.setValue(0);
				
				life.setValue(100);
				size.setValue(1);
				deviation.setValue(0.1f);
			}
		};
		btnDefault.setText("Default");
		btnDefault.setTextOffset(0, 0.4f);
		btnDefault.setRoundedRadius(5f);
		this.add(btnDefault);
		
		genericLabel = new Label(new Vector2f(0.02f, 0.05f), "Direction");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.1f), "X");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.2f), "Y");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.3f), "Z");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		xDirSlider = new Slider(0, -1, 1);
		xDirSlider.setBounds(new Vector4f(0.1f, 0.1f, 0.85f, 0.02f));
		this.add(xDirSlider);
		
		yDirSlider = new Slider(1, -1, 1);
		yDirSlider.setBounds(new Vector4f(0.1f, 0.20f, 0.85f, 0.02f));
		this.add(yDirSlider);
		
		zDirSlider = new Slider(0, -1, 1);
		zDirSlider.setBounds(new Vector4f(0.1f, 0.3f, 0.85f, 0.02f));
		this.add(zDirSlider);
		
		
//		genericLabel = new Label(new Vector2f(0.02f, 0.38f), "Velocity");
//		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
//		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.43f), "Speed");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.53f), "PPS");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.63f), "GC");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		
		speed = new Slider(0.5f, 0, 5f);
		speed.setBounds(new Vector4f(0.1f, 0.43f, 0.85f, 0.02f));
		this.add(speed);
		
		pps = new Slider(100, 0, 5000);
		pps.setBounds(new Vector4f(0.1f, 0.53f, 0.85f, 0.02f));
		this.add(pps);
		
		gravityComp = new Slider(0, 0, 0.1f);
		gravityComp.setBounds(new Vector4f(0.1f, 0.63f, 0.85f, 0.02f));
		this.add(gravityComp);
		
		
		
//		genericLabel = new Label(new Vector2f(0.02f, 0.7f), "Scale");
//		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
//		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.75f), "Life");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.85f), "Size");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.95f), "Deviation");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		life = new Slider(500, 0, 1000);
		life.setBounds(new Vector4f(0.1f, 0.75f, 0.85f, 0.02f));
		this.add(life);
		
		size = new Slider(1, 0, 20);
		size.setBounds(new Vector4f(0.1f, 0.85f, 0.85f, 0.02f));
		this.add(size);
		
		deviation = new Slider(0.4f, 0, 1);
		deviation.setBounds(new Vector4f(0.1f, 0.95f, 0.85f, 0.02f));
		this.add(deviation);
		
		
		radiusError = new Slider(0.0f, 0, 50f);
		radiusError.setBounds(new Vector4f(0.1f, 0.98f, 0.85f, 0.02f));
		this.add(radiusError);
	}
	
	
	public Vector3f getTranslation() {
		position.set(xDirSlider.getValue(), yDirSlider.getValue(), zDirSlider.getValue());
		return position;
	}
	
	
	public Vector3f getScale() {
		scale.set(life.getValue(), size.getValue(), deviation.getValue());
		return scale;
	}
	
	
	public Quaternionf getRotation() {
		rotation.identity();
		rotation.rotateLocalX(MyMath.toRadians(speed.getValue()));
		rotation.rotateLocalY(MyMath.toRadians(pps.getValue()));
		rotation.rotateLocalZ(MyMath.toRadians(gravityComp.getValue()));
		return rotation;
	}
	
	
	@Override
	public void update() {
		
		xDirSlider.update();
		yDirSlider.update();
		zDirSlider.update();
		
		speed.update();
		pps.update();
		gravityComp.update();
		
		life.update();
		size.update();
		deviation.update();
	}
}
