package br.fritzen.engine.particles;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Axis3D;
import br.fritzen.engine.core.particles.ParticleSystem;
import br.fritzen.engine.core.particles.ParticleTexture;
import br.fritzen.engine.gui.elements.Button;
import br.fritzen.engine.gui.elements.Label;

public class ParticlesGuiTest extends Game {

	private ParticleSetupPanel panel;
	
	private ParticleSystem particleSystem;
	
	private int currentTex = 0;
	
	private ArrayList<ParticleTexture> texts;
	
	private Label textLabel;
	
	
	public ParticlesGuiTest() {
		super(1280, 720, "Particles Test");
	}

	
	public static void main(String[] args) {
		new ParticlesGuiTest().start();
	}

	
	@Override
	protected void init() {
		
		this.setShadowMap(false);
		this.setAmbientColor(new Vector4f(0.2f));
		
		Scene scene = Game.getScene();
		
		scene.setAmbientLight(new Vector3f(0.2f));
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1920/1080f, 0.01f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 30));
		camera.addGameComponent(new FreeMove(this.getInput(), 0.5f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.3f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		texts = new ArrayList<ParticleTexture>();
		texts.add(new ParticleTexture(new Texture("src/main/resources/particles/snowflake.png"), 1));
		texts.add(new ParticleTexture(new Texture("src/main/resources/particles/particleStar.png"), 1));
		texts.add(new ParticleTexture(new Texture("src/main/resources/particles/cosmic.png"), 4));
		texts.add(new ParticleTexture(new Texture("src/main/resources/particles/fire.png"), 8));
		texts.add(new ParticleTexture(new Texture("src/main/resources/particles/fire2.png"), 4));
		texts.add(new ParticleTexture(new Texture("src/main/resources/particles/smoke.png"), 8));
		texts.add(new ParticleTexture(new Texture("src/main/resources/particles/particleAtlas.png"), 4));
		
		particleSystem = new ParticleSystem(texts.get(0), 100, 0.5f, 0.002f, 500);
		particleSystem.setLifeError(0.8f);
		particleSystem.setSpeedError(0.2f);
		particleSystem.setScaleError(0.8f);
		particleSystem.setDirection(new Vector3f(0, 1, 0));
		particleSystem.setDirectionDeviation(0.4f);
		particleSystem.setRadiusError(0f);
		
		
		GameObject particleEmitter = new GameObject();
		scene.addGameObject(particleEmitter);
		particleEmitter.addGameComponent(particleSystem);
		particleEmitter.addGameComponent(new Axis3D());
		
		panel = new ParticleSetupPanel();
		panel.setBounds(new Vector4f(0.73f, 0.1f, 0.25f, 0.7f));
		this.getGUI().add(panel);
		
		Button nextBtn = new Button(new Vector4f(0.6f, 0.9f, 0.04f, 0.05f), new Vector4f(0.4f), new Vector4f(1f), new Vector4f(0.6f)) {
			
			@Override
			protected void clickAction() {
				updateParticleTexture(1);
			}
		};
		nextBtn.setText(">>");
		nextBtn.setRoundedRadius(5f);
		nextBtn.setTextOffset(0, 0.3f);
		this.getGUI().add(nextBtn);
		
		Button beforeBtn = new Button(new Vector4f(0.4f, 0.9f, 0.04f, 0.05f), new Vector4f(0.4f), new Vector4f(1f), new Vector4f(0.6f)) {
			
			@Override
			protected void clickAction() {
				updateParticleTexture(-1);
			}
		};
		beforeBtn.setText("<<");
		beforeBtn.setRoundedRadius(5f);
		beforeBtn.setTextOffset(0, 0.3f);
		this.getGUI().add(beforeBtn);
		
		
		textLabel = new Label(new Vector2f(0.475f, 0.92f), "Selected Texture: 0");
		textLabel.setTextColor(new Vector4f(1));
		this.getGUI().add(textLabel);
	}

	
	
	protected void updateParticleTexture(int dir) {
		
		if (dir == 1) {
			if (currentTex < texts.size() -1)
				currentTex++;
			else
				currentTex = 0;
		} else if (dir == -1) {
			if (currentTex > 0)
				currentTex--;
			else
				currentTex = texts.size() - 1;
		}
	
		textLabel.setText("Selected Texture: " + currentTex);
		particleSystem.setTexture(texts.get(currentTex));
		
	}

	
	@Override
	public void input() {
		super.input();
		
		particleSystem.getDirection().x = panel.xDirSlider.getValue();
		particleSystem.getDirection().y = panel.yDirSlider.getValue();
		particleSystem.getDirection().z = panel.zDirSlider.getValue();
		
		particleSystem.setSpeed(panel.speed.getValue());
		particleSystem.setPps(panel.pps.getValue());
		particleSystem.setGravityComplient(panel.gravityComp.getValue());
		particleSystem.setLifeLength(panel.life.getValue());
		
		particleSystem.setScale(panel.size.getValue());
		particleSystem.setDirectionDeviation(panel.deviation.getValue());
		particleSystem.setRadiusError(panel.radiusError.getValue());
		
		if (getInput().getClickedKey(GLFW.GLFW_KEY_TAB)) {
			if (particleSystem.blendMode == GL11.GL_ONE) {
				particleSystem.blendMode = GL11.GL_ONE_MINUS_SRC_ALPHA;
			} else {
				particleSystem.blendMode = GL11.GL_ONE;
			}
		}
	}
	
}
