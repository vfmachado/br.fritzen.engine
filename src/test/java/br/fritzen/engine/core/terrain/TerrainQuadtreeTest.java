package br.fritzen.engine.core.terrain;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.sceneobjects.ISky;
import br.fritzen.engine.core.gameobject.sceneobjects.Skydome;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.rendering.FogModel;
import br.fritzen.engine.terrain.Terrain;
import br.fritzen.engine.utils.MyMath;

public class TerrainQuadtreeTest extends Game {

	
	public static void main(String[] args) {
		new TerrainQuadtreeTest(1280, 720, "Terrain QuadTree").start();
	}
	
	
	public TerrainQuadtreeTest(int width, int height, String title) {
		super(width, height, title);
	}

	
	@Override
	protected void init() {
		
		this.setShadowMap(false);
		
		
		Scene scene = Game.getScene();
		
		scene.setAmbientLight(new Vector3f(0.8f));
		
		Camera camera = new Camera(MyMath.toRadians(70), 1280f/720f, 0.1f, 20000f);
		camera.addGameComponent(new FreeMove(this.getInput(), 2f));
		camera.addGameComponent(new FreeLook(this.getInput(), GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		camera.getTransform().setPosition(new Vector3f(0, 100, 0));
		scene.setCamera(camera);
		
		ISky sky = new Skydome(scene, 3000f);
		scene.setSky(sky);
		
		Terrain terrain = new Terrain();
		terrain.init("src/main/resources/terrain/terrainConfig.txt");
		scene.setTerrain(terrain);
		
		
//		Mesh mesh = new Mesh("src/main/resources/models/cube/model.obj");
//		Material material = new Material().setTexture(terrain.getTerrainConfig().getNormalMap());
//		GameObject cube = new GameObject("cube");
//		cube.getTransform().setScale(new Vector3f(50f));
//		cube.addGameComponent(new MeshRenderer(mesh, material));
//		scene.addGameObject(cube);
		
	}

	
	@Override
	protected void update() {
		super.update();
		
	}
	
}
