package br.fritzen.engine.core.gameobject;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.Attenuation;
import br.fritzen.engine.core.gameobject.lights.SpotLight;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.utils.MyMath;

public class NormalMappingTest extends Game {

	GameObject wall1;
	GameObject wall2;
	
	GameObject rock1;
	GameObject rock2;
	
	public static void main(String[] args) {

		NormalMappingTest program = new NormalMappingTest(1280, 720, "Normal Mapping");
		program.start();
		
	}

	
	public NormalMappingTest(int width, int height, String title) {
		super(width, height, title);
		
	}

	
	@Override
	protected void init() {
		this.setShadowMap(false);
		Scene scene = this.getScene();
		
		scene.setAmbientLight(new Vector3f(0.4f, 0.4f, 0.4f));
		scene.setSunLight(new SunLight(new Vector3f(1, 1, 1), 1.0f, new Vector3f(-0.6f, -0.8f, -0.6f)));
		
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1280f/720f, 0.1f, 100.0f);
		camera.addGameComponent(new FreeMove(this.getInput(), 0.1f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.1f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		camera.getTransform().setPosition(new Vector3f(2.5f, 5, 15));
		scene.setCamera(camera);
	
		Attenuation atten = new Attenuation(0.8f, 0.3f, 0.05f);
		SpotLight spotLight = new SpotLight(new Vector3f(0.5f, 0, 0), 30f, atten, 0.02f);
		
		GameObject spot1 = new GameObject();
		spot1.addGameComponent(spotLight);
		spot1.getTransform().setPosition(new Vector3f(0, 7, 8));
		spot1.getTransform().rotate(GS.X_AXIS, MyMath.toRadians(-90f));
		//scene.addGameObject(spot1);
		
		
		
		//TEST 1
		Mesh mesh1 = new Mesh("src/main/resources/models/plane/plane.obj");
		Material material1 = new Material();
		material1.setTexture(new Texture("src/main/resources/models/bricks/bricks2.jpg"));
		material1.setNormalMap(new Texture("src/main/resources/models/bricks/bricks_normal.png"));
		
		wall1 = new GameObject("obj1");
		wall1.addGameComponent(new MeshRenderer(mesh1, material1));
		scene.addGameObject(wall1);
		

		Material material2 = new Material();
		material2.setTexture(new Texture("src/main/resources/models/bricks/bricks2.jpg"));
	
		wall2 = new GameObject("obj2");
		wall2.addGameComponent(new MeshRenderer(mesh1, material2));
		wall2.getTransform().getPosition().x += 20;
		scene.addGameObject(wall2);
	
		
		wall1.getTransform().setScale(new Vector3f(10f));
		wall2.getTransform().setScale(new Vector3f(10f));
		
		wall1.getTransform().rotate(GS.X_AXIS, MyMath.toRadians(90f));
		wall2.getTransform().rotate(GS.X_AXIS, MyMath.toRadians(90f));
		
		wall1.getTransform().rotate(GS.Z_AXIS, MyMath.toRadians(90f));
		wall2.getTransform().rotate(GS.Z_AXIS, MyMath.toRadians(90f));
		
		
		//TEST 2
		Mesh mesh2 = new Mesh("src/main/resources/models/rock/boulder.obj");
		Material material3 = new Material();
		material3.setTexture(new Texture("src/main/resources/models/rock/boulder.png"));
		material3.setNormalMap(new Texture("src/main/resources/models/rock/boulderNormal.png"));
		
		rock1 = new GameObject("obj1");
		rock1.addGameComponent(new MeshRenderer(mesh2, material3));
		scene.addGameObject(rock1);
		

		Material material4 = new Material();
		material4.setTexture(new Texture("src/main/resources/models/rock/boulder.png"));
	
		rock2 = new GameObject("obj2");
		rock2.addGameComponent(new MeshRenderer(mesh2, material4));
		rock2.getTransform().getPosition().x += 20;
		scene.addGameObject(rock2);
		
		
		rock1.getTransform().setScale(new Vector3f(0.25f));
		rock2.getTransform().setScale(new Vector3f(0.25f));
	
		rock1.getTransform().getPosition().z = 5;
		rock2.getTransform().getPosition().z = 5;
	}
	
	
	@Override
	protected void update() {
		super.update();
		
		rock1.getTransform().rotate(GS.Y_AXIS, MyMath.toRadians(0.2f));
		rock2.getTransform().rotate(GS.Y_AXIS, MyMath.toRadians(0.2f));
	}

	
	@Override
	protected void input() {
		super.input();
	}
}
