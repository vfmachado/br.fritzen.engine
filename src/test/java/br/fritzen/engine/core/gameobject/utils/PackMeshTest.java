package br.fritzen.engine.core.gameobject.utils;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.sceneobjects.ISky;
import br.fritzen.engine.core.gameobject.sceneobjects.Skydome;
import br.fritzen.engine.model.IndexedModel;
import br.fritzen.engine.rendering.FogModel;

public class PackMeshTest extends Game {

	private GameObject object;
	
	
	public static void main(String[] args) {

		PackMeshTest program = new PackMeshTest(720, 480, "OBJ LOADER");
		program.start();
		
	}

	
	public PackMeshTest(int width, int height, String title) {
		super(width, height, title);
		
	}

	
	@Override
	protected void init() {
		
		GS.OBJLOADER_DEBUG = true;
		
		this.setShadowMap(false);
		this.setAmbientColor(new Vector4f(0.02f));
		
		Scene scene = Game.getScene();
		
		ISky sky = new Skydome(scene, 250f);
		scene.setSky(sky);
		
		FogModel fog = new FogModel(0.01f, 2f, new Vector3f(0.835f, 1.0f, 1.0f));
		//scene.setFogModel(fog);
		
		scene.setAmbientLight(new Vector3f(0.8f));
		SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1, new Vector3f(0.5f, -1f, -0.5f));
		scene.setSunLight(sun);
		
		Camera camera = new Camera((float) Math.toRadians(70f), 720f/480f, 0.01f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 30));
		camera.addGameComponent(new FreeMove(this.getInput(), 0.25f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.2f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		Mesh mesh = new Mesh("src/main/resources/models/grass/grass.obj");
		IndexedModel model = mesh.getModel();
		Material material = new Material().setTexture(new Texture("src/main/resources/models/grass/grass.png"));
		object = new GameObject("Object01");
		
		MeshPack meshPack = new MeshPack(model, material);
		
		meshPack.setRadius(1000);
		meshPack.setQuantity(200_000);
		//meshPack.setRandomRotation(false);
		meshPack.pack();
		
		object.addGameComponent(meshPack);
		
		scene.addGameObject(object);
		
		
		GameObject floor = new GameObject();
		floor.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/plane/plane.obj"), new Material().setTexture(new Texture("src/main/resources/images/grass.png"))));
		floor.getTransform().setScale(new Vector3f(500, 1, 500));
		floor.getTransform().setPosition(new Vector3f(0, -1, 0));
		scene.addGameObject(floor);
	}
	
	
	@Override
	protected void update() {
		super.update();
		//object.getTransform().getRotation().rotateAxis((float)Math.toRadians(0.25f), GS.Y_AXIS);
	
	}
	
}
