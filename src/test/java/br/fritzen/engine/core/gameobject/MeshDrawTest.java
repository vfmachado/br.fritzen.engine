package br.fritzen.engine.core.gameobject;

import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.model.Vertex;


public class MeshDrawTest extends Game {

	private float timer = 0;

	private float Z = 0.0f;
	
	private Camera camera;
	
	private GameObject plane;	
	
	public static void main(String[] args) {
		new MeshDrawTest(720, 480, "TESTE").start();
	}

	
	public MeshDrawTest(int width, int height, String title) {
		super(width, height, title);
	}

		
	@Override
	protected void init() {
		
		Scene scene = this.getScene();
		
		camera = new Camera((float) Math.toRadians(70f), 720f/480f, 0.01f, 100.0f);
		camera.getTransform().setPosition(new Vector3f(0, 0, 10));
		scene.setCamera(camera);
		
		Vertex[] vertices = new Vertex[4];
		vertices[0] = new Vertex(new Vector3f(-05f, 05f, 0), new Vector2f(0, 0));
		vertices[1] = new Vertex(new Vector3f(-05f, -05f, 0), new Vector2f(0, 1f));
		vertices[2] = new Vertex(new Vector3f(05f, 05f, 0), new Vector2f(1f, 0));
		vertices[3] = new Vertex(new Vector3f(05f, -05f, 0), new Vector2f(1f, 1f));
		
		int[] indices = {0, 1, 2, 1, 2, 3};
		
		Mesh mesh = new Mesh(vertices, indices);
		
		
		Vertex[] vertices2 = new Vertex[4];
		vertices2[0] = new Vertex(new Vector3f(-05f, 05f, 05), new Vector2f(0, 0));
		vertices2[1] = new Vertex(new Vector3f(-05f, -05f, 05), new Vector2f(0, 1f));
		vertices2[2] = new Vertex(new Vector3f(05f, 05f, 05), new Vector2f(1f, 0));
		vertices2[3] = new Vertex(new Vector3f(05f, -05f, 05), new Vector2f(1f, 1f));
		
		int[] indices2 = {0, 1, 2, 1, 2, 3};
		
		Mesh mesh2 = new Mesh(vertices2, indices2);
		
		
		Texture texture = new Texture("src/main/resources/images/defaultTexture.png");
		Material material = new Material();
		material.setTexture(texture);
		
		Material materialBlank = new Material();
		
		plane = new GameObject();
		plane.addGameComponent(new MeshRenderer(mesh, material));
		plane.addGameComponent(new MeshRenderer(mesh2, materialBlank));
		
		scene.addGameObject(plane);
		
	}

	
	@Override
	protected void update() {
		super.update();
		
		timer += 0.01f;
		
		if (timer >= 1f) {
			
			timer = 0.0f;
			Vector4f color = new Vector4f(1f);
			
			Random r = new Random();
			color.x = r.nextFloat();
			color.y = r.nextFloat();
			color.z = r.nextFloat();
			
			this.setAmbientColor(color);
		}
		
		Z = 1f;
		//this.camera.getTransform().setPosition(new Vector3f(0, 0, Z));
		this.plane.getTransform().getRotation().rotateAxis((float) Math.toRadians(Z), new  Vector3f(1, 0, 0));
	}
	
	
}
