package br.fritzen.engine.core.gameobject.utils;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameComponent;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.Attenuation;
import br.fritzen.engine.core.gameobject.lights.PointLight;
import br.fritzen.engine.core.gameobject.lights.SpotLight;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.sceneobjects.ISky;
import br.fritzen.engine.core.gameobject.sceneobjects.Skydome;
import br.fritzen.engine.formats.collada.ColladaLoader;
import br.fritzen.engine.formats.collada.ColladaModel;
import br.fritzen.engine.formats.collada.libraries.ColladaGeometry;
import br.fritzen.engine.gui.elements.Button;
import br.fritzen.engine.gui.elements.Panel;
import br.fritzen.engine.model.IndexedModel;
import br.fritzen.engine.model.Vertex;
import br.fritzen.engine.terrain.Terrain;

public class SceneExample extends Game {

	private GameObject cube;
	private Material blank = new Material();;
	
	GameObject point1;
	GameObject point2;
	GameObject point3;
	GameObject spotLight;
	GameObject spotLight2;
	GameObject spotLight3;
	
	Attenuation atten = new Attenuation();
	
	Mesh mesh;
	
	public static void main(String[] args) {

		SceneExample program = new SceneExample(1280, 720, "Scene Example");
		program.start();
		
	}

	
	public SceneExample(int width, int height, String title) {
		super(width, height, title);
		
	}
	
	
	@Override
	protected void init() {

		this.setShadowMap(false);
		
		Scene scene = this.getScene();
		
	
		
		scene.setAmbientLight(new Vector3f(0.4f, 0.4f, 0.4f));
		scene.setSunLight(new SunLight(new Vector3f(1, 1, 1), 1.0f, new Vector3f(0.6f, -0.8f, -0.6f)));
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1280f/720f, 0.1f, 10000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 10));
		camera.getTransform().getRotation().rotate((float)Math.toRadians(-30), 0, 0);
		camera.addGameComponent(new FreeMove(this.getInput(), 0.5f));
		camera.addGameComponent(new FreeLook(this.getInput(), GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		
		ISky sky = new Skydome(scene, 2000f);
		scene.setSky(sky);
		
		
		GameObject arissa = new GameObject();
		
		ColladaLoader loader = new ColladaLoader();
		ColladaModel model = loader.load("src/main/resources/collada/arissa.dae");
		
		for (ColladaGeometry geometry : model.getLibraryGeometries()) {
			IndexedModel indexedModel0 = new IndexedModel(
					geometry.getVertices(),
					geometry.getTexCoords(),
					geometry.getNormals(),
					geometry.getIndicesIndexedModel()
					); 
			Mesh mesh0 = new Mesh(indexedModel0);
			arissa.addGameComponent(new MeshRenderer(mesh0));
			//arissa.addGameComponent(mesh0.getBoundingBox());
		}
		//scene.addGameObject(arissa);
		
		
		Vertex[] vertices = new Vertex[4];
		vertices[0] = new Vertex(new Vector3f(-5, 0, 5), new Vector2f(0, 0), new Vector3f(0, 1, 0), GS.X_AXIS);
		vertices[1] = new Vertex(new Vector3f(-5, 0, -5), new Vector2f(0, 1f), new Vector3f(0, 1, 0), GS.X_AXIS);
		vertices[2] = new Vertex(new Vector3f(5, 0, 5), new Vector2f(1f, 0), new Vector3f(0, 1, 0), GS.X_AXIS);
		vertices[3] = new Vertex(new Vector3f(5, 0, -5), new Vector2f(1f, 1f), new Vector3f(0, 1, 0), GS.X_AXIS);
		
		int[] indices = {0, 1, 2, 1, 2, 3};
		
		Mesh planeMesh = new Mesh(vertices, indices);
		Material material = new Material();
		material.setTexture(new Texture("src/main/resources/images/wood-floor.png"));
		material.setSpecularColor(new Vector3f(1, 1, 1));
		material.setSpecularExponent(1f);
		//material.setDissolveFactor(4f);
		
		GameObject plane = new GameObject("floor");
		plane.addGameComponent(new MeshRenderer(planeMesh, material));
		plane.getTransform().setScale(new Vector3f(5.5f, 1f, 5.5f));
		scene.addGameObject(plane);
		
		mesh = new Mesh("src/main/resources/models/cube/model.obj");
		blank.setSpecularExponent(2f);
		
		Mesh mesh2 = new Mesh("src/main/resources/models/sphere/model.obj");
		
		Mesh mesh3 = new Mesh("src/main/resources/models/dragon/model.obj");
		
		ArrayList<Mesh> meshes = new ArrayList<Mesh>();
		meshes.add(mesh);
		meshes.add(mesh2);
		meshes.add(mesh3);
		meshes.add(mesh2);
		meshes.add(mesh);
		
		//Mesh nanoMesh = new Mesh("src/main/resources/models/nanosuit/nanosuit.obj");
		//GameObject nano = new GameObject("nanosuit");
		//nano.addGameComponent(new MeshRenderer(nanoMesh));
		//nano.getTransform().setScale(new Vector3f(0.2f, 0.2f, 0.2f));
		//cube.getTransform().getRotation().rotateAxis((float)Math.toRadians(new Random().nextInt(366)), GS.Y_AXIS);
		//cube.getTransform().setPosition(new Vector3f(new Random().nextFloat() * 50 - 25, 1, new Random().nextFloat() * 50 -25));
		//nano.addGameComponent(nanoMesh.getBoundingBox());
		//nano.addGameComponent(new Axis3D());
		//this.getScene().addGameObject(nano);
		
		//POINT LIGHTS
		GameObject pointLight1 = new GameObject("Point Light 001");
		GameComponent lightComp1 = new PointLight(GS.X_AXIS, 0.5f, new Attenuation());
		pointLight1.addGameComponent(lightComp1);
		pointLight1.getTransform().setPosition(new Vector3f(-5, 0.2f, 5));
		scene.addGameObject(pointLight1);
				
		
		point1 = new GameObject("ponto-de-luz-1");
		point1.addGameComponent(new PointLight(new Vector3f(1, 0, 0),  8, atten));
		point1.getTransform().setPosition(new Vector3f(0, 0.5f, 1f));
		scene.addGameObject(point1);
		
		point1.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/sphere/model.obj"), 
				new Material().createBlank().setDiffuseColor(new Vector3f(1, 0, 0))));
		
		
		point2 = new GameObject("ponto-de-luz-2");
		point2.addGameComponent(new PointLight(new Vector3f(0, 0, 1),  8, atten));
		point2.getTransform().setPosition(new Vector3f(2, 0.1f, 1f));
		scene.addGameObject(point2);
		
		point2.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/sphere/model.obj"), 
				new Material().createBlank().setDiffuseColor(new Vector3f(0, 0, 1))));
		
		point3 = new GameObject("ponto-de-luz-3");
		point3.addGameComponent(new PointLight(new Vector3f(0, 1, 0),  8, atten));
		point3.getTransform().setPosition(new Vector3f(-2, 1f, 1f));
		scene.addGameObject(point3);
		
		point3.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/sphere/model.obj"), 
				new Material().createBlank().setDiffuseColor(new Vector3f(0, 1, 0))));
		
		
		spotLight = new GameObject("luz-spot-4");
		spotLight.addGameComponent(new SpotLight(new Vector3f(1, 0, 0), 100, atten, 0.3f));
		//scene.addGameObject(spotLight);
		spotLight.getTransform().setPosition(new Vector3f(0, 20f, -8f));
		spotLight.getTransform().getRotation().rotateAxis( -(float) Math.PI / 2, new Vector3f(1, 0, 0));
		spotLight.getTransform().getRotation().rotateAxis((float)Math.toRadians(60f), GS.Y_AXIS);
		
		
		Material cubeMaterial = new Material().createBlank();
		cubeMaterial.setDiffuseColor(new Vector3f(1, 0, 0));
		Mesh cubeMesh = new Mesh("src/main/resources/models/cube/model.obj");
		spotLight.addGameComponent(new MeshRenderer(cubeMesh, cubeMaterial));
		spotLight.getTransform().setScale(new Vector3f(0.4f, 0.4f, 0.1f));
		
		
		spotLight2 = new GameObject("luz-spot-5");
		spotLight2.addGameComponent(new SpotLight(GS.Y_AXIS, 100f, atten, 0.3f));
		//scene.addGameObject(spotLight2);
		
		spotLight2.getTransform().setPosition(new Vector3f(8, 20f, 8));
		spotLight2.getTransform().getRotation().rotateAxis( -(float) Math.PI / 2, new Vector3f(1, 0, 0));
		spotLight2.getTransform().getRotation().rotateAxis((float)Math.toRadians(60f), GS.Y_AXIS);
		spotLight2.addGameComponent(new MeshRenderer(cubeMesh, cubeMaterial));
		spotLight2.getTransform().setScale(new Vector3f(0.4f, 0.4f, 0.1f));
		
		
		spotLight3 = new GameObject("luz-spot-5");
		spotLight3.addGameComponent(new SpotLight(new Vector3f(0f, 0f, 1), 100f, atten, 0.3f));
		//scene.addGameObject(spotLight3);
		
		spotLight3.getTransform().setPosition(new Vector3f(-8, 20f, 8));
		spotLight3.getTransform().getRotation().rotateAxis( -(float) Math.PI / 2, new Vector3f(1, 0, 0));
		spotLight3.getTransform().getRotation().rotateAxis((float)Math.toRadians(60f), GS.Y_AXIS);
		spotLight3.addGameComponent(new MeshRenderer(cubeMesh, cubeMaterial));
		spotLight3.getTransform().setScale(new Vector3f(0.4f, 0.4f, 0.1f));
		
		ArrayList<Button> btns = new ArrayList<Button>();
				
		for (int i = 1; i <= 5; i++) {
			final Integer innerMi = new Integer(i) -1;
			Button btnCube = new Button(new Vector4f(0.15f * i, 0.85f, 0.1f, 0.1f), new Vector4f(0.3f, 0.3f, 0.3f, 1f), new Vector4f(1f, 1f, 1f, 1f), new Vector4f(0.1f, 0.1f, 0.1f, 1f) ) {
				
				@Override
				protected void clickAction() {
					
					addRandomCube(meshes.get(innerMi));
				}
			};
			btnCube.setText("Add a cube");
			//btnCube.setTextOffset(0.2f, 0.4f);
			btnCube.setRoundedRadius(5f);
			this.getGUI().add(btnCube);
			
			btns.add(btnCube);
		}
		
		
		btns.get(1).setText("Add a Sphere");
		btns.get(2).setText("Add a Dragon");
		btns.get(3).setText("Add a Sphere");

	}
	
	
	public void addRandomCube(Mesh mesh) {
		
		GameObject cube = new GameObject("random");
		cube.addGameComponent(new MeshRenderer(mesh));
		cube.getTransform().setScale(new Vector3f(1f));
		cube.getTransform().getRotation().rotateAxis((float)Math.toRadians(new Random().nextInt(366)), GS.Y_AXIS);
		cube.getTransform().setPosition(new Vector3f(new Random().nextFloat() * 50 - 25, 1, new Random().nextFloat() * 50 -25));
		cube.addGameComponent(mesh.getBoundingBox());
		cube.addGameComponent(new Axis3D());
		this.getScene().addGameObject(cube);
	
	}
	
	
	float temp = 0.0f;
	float temp2 = 0.0f;
	
	@Override
	protected void update() {
		super.update();

		//System.out.println(this.getScene().getCamera().getTransform().getPosition());
		//cube.getTransform().getRotation().rotateAxis((float)Math.toRadians(0.5f), GS.Y_AXIS);
		//cube.getTransform().getPosition().add(cube.getTransform().getForward());
		
		
		temp += 0.5f;
		
		
		if (temp >= 0 && temp < 100) {
			spotLight.getTransform().getRotation().rotateAxis((float)Math.toRadians(-0.6f), GS.Y_AXIS);
			spotLight2.getTransform().getRotation().rotateAxis((float)Math.toRadians(-0.6f), GS.Y_AXIS);
			spotLight3.getTransform().getRotation().rotateAxis((float)Math.toRadians(-0.6f), GS.Y_AXIS);
		}
		else if (temp < 0) {
			spotLight.getTransform().getRotation().rotateAxis((float)Math.toRadians(+0.6f), GS.Y_AXIS);
			spotLight2.getTransform().getRotation().rotateAxis((float)Math.toRadians(+0.6f), GS.Y_AXIS);
			spotLight3.getTransform().getRotation().rotateAxis((float)Math.toRadians(+0.6f), GS.Y_AXIS);
		} else {
			temp = -100f;
		}
		
		
		temp2 += 0.03f;
		point1.getTransform().getPosition().x = 25 * (float) Math.sin(temp2/10);
		point1.getTransform().getPosition().z = 25 * (float) Math.cos(temp2/10);
		
		point2.getTransform().getPosition().x = 25 * (float) Math.sin(temp2/5);
		point2.getTransform().getPosition().z = 25 * (float) Math.cos(temp2/5);
		
		point3.getTransform().getPosition().x = 25 * (float) Math.sin(temp2/15);
		point3.getTransform().getPosition().z = 25 * (float) Math.cos(temp2/15);
		
		
	}
	
	float specular = 2.0f;
	@Override
	protected void input() {
		super.input();
		
		if (this.getInput().getKey(GLFW.GLFW_KEY_1)) {
			atten.setConstant(atten.getConstant() - 0.01f);
		} else if (this.getInput().getKey(GLFW.GLFW_KEY_2)) {
			atten.setConstant(atten.getConstant() + 0.01f);
		} else if (this.getInput().getKey(GLFW.GLFW_KEY_3)) {
			atten.setLinear(atten.getLinear() - 0.01f);
		} else if (this.getInput().getKey(GLFW.GLFW_KEY_4)) {
			atten.setLinear(atten.getLinear() + 0.01f);
		} else if (this.getInput().getKey(GLFW.GLFW_KEY_5)) {
			atten.setExponent(atten.getExponent() - 0.01f);
		} else if (this.getInput().getKey(GLFW.GLFW_KEY_6)) {
			atten.setExponent(atten.getExponent() + 0.01f);
		} 
		
		if (this.getInput().getKey(GLFW.GLFW_KEY_EQUAL)) {
			specular += 0.01f;
		}
		if (this.getInput().getKey(GLFW.GLFW_KEY_MINUS)) {
			specular -= 0.01f;
		}
		//System.out.println(specular);
		blank.setSpecularExponent(specular);
		
		//Vector2f mousePos = this.getInput().getMousePosition();
		//System.out.println(mousePos);
		System.out.println("Current Atten: " + atten.toString());
	}
}
