package br.fritzen.engine.core.gameobject.utils;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;

public class OBJLoaderTest001 extends Game {

	private GameObject object;
	
	
	public static void main(String[] args) {

		OBJLoaderTest001 program = new OBJLoaderTest001(720, 480, "OBJ LOADER");
		program.start();
		
	}

	
	public OBJLoaderTest001(int width, int height, String title) {
		super(width, height, title);
		
	}

	
	@Override
	protected void init() {
		
		GS.OBJLOADER_DEBUG = true;
		
		this.setShadowMap(false);
		this.setAmbientColor(new Vector4f(0.02f));
		
		Scene scene = Game.getScene();
		
		scene.setAmbientLight(new Vector3f(0.8f));
		SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1, new Vector3f(0.5f, -1f, -0.5f));
		scene.setSunLight(sun);
		
		Camera camera = new Camera((float) Math.toRadians(70f), 720f/480f, 0.01f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 30));
		camera.addGameComponent(new FreeMove(this.getInput(), 0.25f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.2f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		Mesh mesh = new Mesh("src/main/resources/models/grass/grass.obj");
		object = new GameObject("Object01");
		object.addGameComponent(new MeshRenderer(mesh));
//		object.addGameComponent(mesh.getBoundingBox());
		scene.addGameObject(object);
			
	}
	
	
	@Override
	protected void update() {
		super.update();
		object.getTransform().getRotation().rotateAxis((float)Math.toRadians(0.25f), GS.Y_AXIS);
	
	}
	
}
