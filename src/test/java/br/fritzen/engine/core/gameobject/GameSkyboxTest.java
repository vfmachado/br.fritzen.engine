package br.fritzen.engine.core.gameobject;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.sceneobjects.ISky;
import br.fritzen.engine.core.gameobject.sceneobjects.Skydome;

public class GameSkyboxTest extends Game {

	public static void main(String[] args) {

		GameSkyboxTest program = new GameSkyboxTest(1920, 1080, "Scene Example");
		program.start();
		
	}

	
	public GameSkyboxTest(int width, int height, String title) {
		super(width, height, title);
		
	}
	
	
	@Override
	protected void init() {

		Scene scene = this.getScene();
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1920/1080f, 0.01f, 100.0f);
		camera.getTransform().setPosition(new Vector3f(0, 0, 5));
		//camera.getTransform().getRotation().rotate((float)Math.toRadians(-30), 0, 0);
		camera.addGameComponent(new FreeMove(this.getInput(), 0.25f));
		camera.addGameComponent(new FreeLook(this.getInput(), GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		ISky sky = new Skydome(scene, 50f);
		scene.setSky(sky);
		
		
	}
	
	
	@Override
	protected void update() {
		super.update();
	}
	
	
	@Override
	protected void input() {
		super.input();
		
	}
}
