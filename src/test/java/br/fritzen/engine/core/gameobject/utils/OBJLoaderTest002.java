package br.fritzen.engine.core.gameobject.utils;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.gui.elements.Label;
import br.fritzen.engine.rendering.FogModel;

public class OBJLoaderTest002 extends Game {

	private GameObject object;
	
	private ArrayList<Mesh> meshes;
	
	private Label textLabel;
	
	
	public static void main(String[] args) {

		OBJLoaderTest002 program = new OBJLoaderTest002(720, 480, "OBJ LOADER");
		program.start();
		
	}

	
	public OBJLoaderTest002(int width, int height, String title) {
		super(width, height, title);
		
	}

	
	@Override
	protected void init() {
		
		this.setShadowMap(false);
		this.setAmbientColor(new Vector4f(0.0f));
		
		Scene scene = Game.getScene();
		
		FogModel fog = new FogModel(0.01f, 2f, new Vector3f(0.164f, 0.254f, 0.458f));
		scene.setFogModel(fog);
		
		scene.setAmbientLight(new Vector3f(0.2f));
		SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1, new Vector3f(0.5f, -1f, -0.5f));
		scene.setSunLight(sun);
		
		Camera camera = new Camera((float) Math.toRadians(70f), 720f/480f, 0.01f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 30));
		camera.addGameComponent(new FreeMove(this.getInput(), 0.05f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.3f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		
		//TEST 1
		Mesh mesh1 = new Mesh("src/main/resources/models/plane/plane.obj");
		Material material1 = new Material();
		material1.setTexture(new Texture("src/main/resources/images/defaultTexture.png"));
		material1.setSpecularExponent(1);
		GameObject floor = new GameObject("obj1");
		floor.addGameComponent(new MeshRenderer(mesh1, material1));
		floor.getTransform().setScale(new Vector3f(50, 1, 50));
		floor.getTransform().setPosition(new Vector3f(0, -10, 0));
		scene.addGameObject(floor);
		
		
		meshes = new ArrayList<Mesh>();
		meshes.add(new Mesh("src/main/resources/models/cube/model.obj"));
		meshes.add(new Mesh("src/main/resources/models/cartoon_girl/model.obj"));
		meshes.add(new Mesh("src/main/resources/models/barrel/barrel.obj"));
		meshes.add(new Mesh("src/main/resources/models/dragon/model.obj"));
		meshes.add(new Mesh("src/main/resources/models/rock/boulder.obj"));
		meshes.add(new Mesh("src/main/resources/models/bush_01/palm.obj"));
		
		object = new GameObject("Object01");
		object.addGameComponent(new MeshRenderer(meshes.get(0)));
		
		scene.addGameObject(object);
				
		
		textLabel = new Label(new Vector2f(0.05f, 0.95f), "Selected Mesh: 0");
		this.getGUI().add(textLabel);
	}
	
	
	@Override
	protected void update() {
		super.update();
		object.getTransform().getRotation().rotateAxis((float)Math.toRadians(0.5f), GS.Y_AXIS);
		
		
	}
	
	int selectedMesh = 0;
	
	@Override
	protected void input() {
		super.input();
		
		if (this.getInput().getClickedKey(GLFW.GLFW_KEY_RIGHT)) {
			
			selectedMesh++;
			
			if (selectedMesh >= meshes.size()) {
				selectedMesh = 0;
			}
			
			textLabel.setText("Selected Mesh: " + selectedMesh);
			
		} else if (this.getInput().getClickedKey(GLFW.GLFW_KEY_LEFT)) {
			selectedMesh--;
			
			if (selectedMesh < 0) {
				selectedMesh = meshes.size() -1;
			}
			
			textLabel.setText("Selected Mesh: " + selectedMesh);
		}
		
		((MeshRenderer) object.getComponentsByName(GS.COMP_MESH_RENDERER).get(0)).setMesh(meshes.get(selectedMesh));

	}
}
