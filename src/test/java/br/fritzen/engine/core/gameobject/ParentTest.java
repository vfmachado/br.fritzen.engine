package br.fritzen.engine.core.gameobject;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Axis3D;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.utils.MyMath;

public class ParentTest  extends Game {

	GameObject cube;
	GameObject dad;
	GameObject child;
	
	public static void main(String[] args) {

		ParentTest program = new ParentTest(1920, 1080, "Parent Test");
		program.start();
		
	}

	
	public ParentTest(int width, int height, String title) {
		super(width, height, title);
		
	}
	
	
	@Override
	protected void init() {

		this.setShadowMap(false);
		//this.setAmbientColor(new Vector4f(0.8f));
		
		Scene scene = this.getScene();
		//scene.setAmbientLight(new Vector3f(0.8f));
		//SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1f, new Vector3f(0.5f, -1f, -0.8f));
		//scene.setSunLight(sun);
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1920/1080f, 0.01f, 100.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 15));
		camera.getTransform().getRotation().rotate(MyMath.toRadians(-30), 0, 0);
		
		camera.addGameComponent(new FreeMove(this.getInput(), 0.2f));
		camera.addGameComponent(new FreeLook(this.getInput(), GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
		
		
		cube = new GameObject("cube");
		cube.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/cube/model.obj")));
		cube.addGameComponent(new FreeMove(this.getInput(), 0.1f, GLFW.GLFW_KEY_UP, GLFW.GLFW_KEY_LEFT, GLFW.GLFW_KEY_DOWN, GLFW.GLFW_KEY_RIGHT, GLFW.GLFW_KEY_KP_ADD, GLFW.GLFW_KEY_KP_SUBTRACT));
		cube.addGameComponent(new Axis3D());
		scene.addGameObject(cube);
		
		dad = new GameObject("dad");
		dad.addGameComponent(new Axis3D());
		dad.getTransform().setPosition(new Vector3f(0, 0, -5));
		cube.addChild(dad);
		
		child = new GameObject("child");
		child.addGameComponent(new Axis3D());
		child.getTransform().setPosition(new Vector3f(4, 0, 0));
		dad.addChild(child);
				
	}
	
	
	@Override
	protected void update() {
		super.update();
		cube.getTransform().getRotation().rotateAxis(MyMath.toRadians(0.5f), GS.Y_AXIS);
	}
	
	
	@Override
	protected void input() {
		super.input();
		
	}

}
