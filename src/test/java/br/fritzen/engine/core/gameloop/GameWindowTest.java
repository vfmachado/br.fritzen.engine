package br.fritzen.engine.core.gameloop;

import java.util.Random;

import org.joml.Vector4f;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.utils.MyMath;

/**
 * 
 * Basic game Window with random background colors
 * 
 * @author Vinicius Fritzen Machao
 */
public class GameWindowTest extends Game {

	private float timer = 0;
	
	
	public static void main(String[] args) {
		new GameWindowTest(720, 480, "TESTE").start();
	}

	
	public GameWindowTest(int width, int height, String title) {
		super(width, height, title);
	}

		
	@Override
	protected void init() {
		
		this.setShadowMap(false);
		
		Camera camera = new Camera(MyMath.toRadians(60f), 1.4f, 0.001f, 100f);
		Game.getScene().setCamera(camera);
		
		this.setAmbientColor(new Vector4f(1.0f, 0.0f, 0.0f, 1.0f));
		
	}
	
	Vector4f color = new Vector4f(1f);
	Random r = new Random();
	
	@Override
	protected void update() {
		super.update();
		timer += 0.01f;
		
		if (timer >= 1f) {
			
			timer = 0.0f;
			color.x = r.nextFloat();
			color.y = r.nextFloat();
			color.z = r.nextFloat();
			
			this.setAmbientColor(color);
		}
	}
	
}
