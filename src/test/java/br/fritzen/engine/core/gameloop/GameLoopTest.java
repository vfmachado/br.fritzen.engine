package br.fritzen.engine.core.gameloop;

/**
 * 
 * Simple test to UPS and FPS mechanism
 * 
 * @author Vinicius Fritzen Machado
 */
public class GameLoopTest extends GameLoop {

	public static void main(String[] args) {
		new GameLoopTest().run();
	}
	
	@Override
	protected void init() {
		
	}

	@Override
	protected void input() {
		
	}

	@Override
	protected void update() {
		
	}

	@Override
	protected void render() {
		
	}

}
