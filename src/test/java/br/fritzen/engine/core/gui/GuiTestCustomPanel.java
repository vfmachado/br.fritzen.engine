package br.fritzen.engine.core.gui;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Mesh;


public class GuiTestCustomPanel  extends Game {

	GameObject dragon;
	GameObjectPanel superPanel;
	
	public static void main(String[] args) {

		GuiTestCustomPanel program = new GuiTestCustomPanel(1280, 720, "GuiTest");
		program.start();
		
	}

	
	public GuiTestCustomPanel(int width, int height, String title) {
		super(width, height, title);
		
	}
	
	
	@Override
	protected void init() {

		Scene scene = this.getScene();
		
		scene.setAmbientLight(new Vector3f(0.2f, 0.2f, 0.2f));
		scene.setSunLight(new SunLight(new Vector3f(1, 1, 1), 1.0f, new Vector3f(-0.6f, -0.8f, -0.6f)));
		
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1280/720f, 0.01f, 100.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 15));
		//camera.getTransform().getRotation().rotate(MyMath.toRadians(-30), 0, 0);
		
		camera.addGameComponent(new FreeMove(this.getInput(), 0.1f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.1f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
		
		dragon = new GameObject("dragon");
		dragon.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/dragon/model.obj")));
		scene.addGameObject(dragon);
	
		superPanel = new GameObjectPanel();
		superPanel.setBounds(new Vector4f(0.73f, 0.1f, 0.25f, 0.7f));
		this.getGUI().add(superPanel);
		
	}
	
	
	@Override
	protected void update() {
		
		super.update();
		dragon.getTransform().setPosition(superPanel.getTranslation());
		dragon.getTransform().setScale(superPanel.getScale());
		dragon.getTransform().setRotation(superPanel.getRotation());
	}
	
	
	@Override
	protected void input() {
		super.input();
	}

}
