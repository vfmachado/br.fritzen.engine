package br.fritzen.engine.core.gui;

import org.joml.Quaternionf;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import br.fritzen.engine.gui.GUIElement;
import br.fritzen.engine.gui.elements.Button;
import br.fritzen.engine.gui.elements.Label;
import br.fritzen.engine.gui.elements.Panel;
import br.fritzen.engine.gui.elements.Slider;
import br.fritzen.engine.utils.MyMath;

public class GameObjectPanel extends GUIElement {

	Panel backGroundPanel;
	Label genericLabel;
	
	Slider xPosSlider;
	Slider yPosSlider;
	Slider zPosSlider;
	
	Slider xRotSlider;
	Slider yRotSlider;
	Slider zRotSlider;
	
	Slider xScaSlider;
	Slider yScaSlider;
	Slider zScaSlider;
	
	Button btnDefault;
	
	Vector3f position;
	Vector3f scale;
	Quaternionf rotation;
	
	public GameObjectPanel() {
	
		position = new Vector3f();
		scale = new Vector3f(1);
		rotation = new Quaternionf();
		
		backGroundPanel = new Panel(new Vector4f(0, 0, 1, 1), new Vector4f(0.3f, 0.3f, 0.3f, 0.8f));
		this.add(backGroundPanel);
	
		btnDefault = new Button(new Vector4f(0.7f, 0.005f, 0.29f, 0.05f), new Vector4f(0.2f, 0.2f, 0.2f, 1.0f), new Vector4f(1)) {
			
			@Override
			protected void clickAction() {
				
				xPosSlider.setValue(0);
				yPosSlider.setValue(0);
				zPosSlider.setValue(0);
				
				xRotSlider.setValue(0);
				yRotSlider.setValue(0);
				zRotSlider.setValue(0);
				
				xScaSlider.setValue(1);
				yScaSlider.setValue(1);
				zScaSlider.setValue(1);
			}
		};
		btnDefault.setText("Default");
		btnDefault.setTextOffset(0, 0.4f);
		this.add(btnDefault);
		
		genericLabel = new Label(new Vector2f(0.02f, 0.05f), "Position");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.1f), "X");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.2f), "Y");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.3f), "Z");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		xPosSlider = new Slider(0, -10, 10);
		xPosSlider.setBounds(new Vector4f(0.1f, 0.1f, 0.85f, 0.02f));
		this.add(xPosSlider);
		
		yPosSlider = new Slider(0, -10, 10);
		yPosSlider.setBounds(new Vector4f(0.1f, 0.20f, 0.85f, 0.02f));
		this.add(yPosSlider);
		
		zPosSlider = new Slider(0, -10, 10);
		zPosSlider.setBounds(new Vector4f(0.1f, 0.3f, 0.85f, 0.02f));
		this.add(zPosSlider);
		
		
		genericLabel = new Label(new Vector2f(0.02f, 0.38f), "Rotation");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.43f), "X");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.53f), "Y");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.63f), "Z");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		
		xRotSlider = new Slider(0, 0, 360);
		xRotSlider.setBounds(new Vector4f(0.1f, 0.43f, 0.85f, 0.02f));
		this.add(xRotSlider);
		
		yRotSlider = new Slider(0, 0, 360);
		yRotSlider.setBounds(new Vector4f(0.1f, 0.53f, 0.85f, 0.02f));
		this.add(yRotSlider);
		
		zRotSlider = new Slider(0, 0, 360);
		zRotSlider.setBounds(new Vector4f(0.1f, 0.63f, 0.85f, 0.02f));
		this.add(zRotSlider);
		
		
		
		genericLabel = new Label(new Vector2f(0.02f, 0.7f), "Scale");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.75f), "X");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.85f), "Y");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		genericLabel = new Label(new Vector2f(0.05f, 0.95f), "Z");
		genericLabel.setTextColor(new Vector4f(1, 1, 1, 1));
		this.add(genericLabel);
		
		xScaSlider = new Slider(1, 0, 10);
		xScaSlider.setBounds(new Vector4f(0.1f, 0.75f, 0.85f, 0.02f));
		this.add(xScaSlider);
		
		yScaSlider = new Slider(1, 0, 10);
		yScaSlider.setBounds(new Vector4f(0.1f, 0.85f, 0.85f, 0.02f));
		this.add(yScaSlider);
		
		zScaSlider = new Slider(1, 0, 10);
		zScaSlider.setBounds(new Vector4f(0.1f, 0.95f, 0.85f, 0.02f));
		this.add(zScaSlider);
	}
	
	
	public Vector3f getTranslation() {
		position.set(xPosSlider.getValue(), yPosSlider.getValue(), zPosSlider.getValue());
		return position;
	}
	
	
	public Vector3f getScale() {
		scale.set(xScaSlider.getValue(), yScaSlider.getValue(), zScaSlider.getValue());
		return scale;
	}
	
	
	public Quaternionf getRotation() {
		rotation.identity();
		rotation.rotateLocalX(MyMath.toRadians(xRotSlider.getValue()));
		rotation.rotateLocalY(MyMath.toRadians(yRotSlider.getValue()));
		rotation.rotateLocalZ(MyMath.toRadians(zRotSlider.getValue()));
		return rotation;
	}
	
	
	@Override
	public void update() {
		
		xPosSlider.update();
		yPosSlider.update();
		zPosSlider.update();
		
		xRotSlider.update();
		yRotSlider.update();
		zRotSlider.update();
		
		xScaSlider.update();
		yScaSlider.update();
		zScaSlider.update();
	}
}
