package br.fritzen.engine.core.gui;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Random;

import org.joml.FrustumIntersection;
import org.joml.Matrix4f;
import org.joml.RayAabIntersection;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Axis3D;
import br.fritzen.engine.core.gameobject.utils.BoundingBox;
import br.fritzen.engine.core.gameobject.utils.Line3D;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.core.gameobject.utils.Utils;
import br.fritzen.engine.gui.elements.GridPanel;
import br.fritzen.engine.gui.elements.Label;
import br.fritzen.engine.gui.elements.Panel;
import br.fritzen.engine.gui.elements.Slider;
import br.fritzen.engine.rendering.FogModel;
import br.fritzen.engine.utils.MyMath;


public class GuiTest  extends Game {

	GameObject cube;
	GameObject line;
	Slider slider;
	GameObjectPanel superPanel;
	Mesh cubeMesh;
	ArrayList<BoundingBox> boxes;
	Camera camera; 
	
	
	public static void main(String[] args) {

		GuiTest program = new GuiTest(1280, 720, "GuiTest");
		program.start();
		
	}

	
	public GuiTest(int width, int height, String title) {
		super(width, height, title);
		
	}
	
	
	@Override
	protected void init() {
		
		this.setShadowMap(true);

		Scene scene = this.getScene();
		
		FogModel fog = new FogModel(0.01f, 2f, new Vector3f(0.4f));
		scene.setFogModel(fog);
		
		scene.setAmbientLight(new Vector3f(0.2f, 0.2f, 0.2f));
		SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1f, new Vector3f(0.5f, -1f, -0.8f));
		scene.setSunLight(sun);
		
		camera = new Camera((float) Math.toRadians(70f), 1280/720f, 0.01f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 10));
		camera.getTransform().getRotation().rotate(MyMath.toRadians(-30), 0, 0);
		
		camera.addGameComponent(new FreeMove(this.getInput(), 0.1f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.1f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
		
		
		//TEST 1
		Mesh mesh1 = new Mesh("src/main/resources/models/plane/plane.obj");
		
		GameObject floor = new GameObject("obj1");
		floor.addGameComponent(new MeshRenderer(mesh1));
		floor.getTransform().setScale(new Vector3f(100, 1, 100));
		scene.addGameObject(floor);
		
		boxes = new ArrayList<BoundingBox>();
		cubeMesh = new Mesh("src/main/resources/models/cube/model.obj");
		
		for (int i = 0; i < 1_00; i++) {
			cube = new GameObject("cube");
			//boxes.add(cubeMesh.getBoundingBox());
			cube.addGameComponent(new MeshRenderer(cubeMesh));
			//cube.addGameComponent(boxes.get(i));
			scene.addGameObject(cube);
			//cube.getTransform().getRotation().rotateAxis((float)Math.toRadians(new Random().nextInt(366)), new Vector3f(new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat()));
			cube.getTransform().setPosition(new Vector3f(new Random().nextFloat() * 200 - 100, new Random().nextFloat() * 100, new Random().nextFloat() * 200 - 100));
			
			//b.transform(b.getParent().getTransform().getTransformation());
			//boxes.get(i).getParent().getTransform().preProcess();
			//boxes.get(i).transform(boxes.get(i).getParent().getTransform().getTransformation());
			//cube.addGameComponent(new Axis3D());
		}
		
		GridPanel gridPanel = new GridPanel(new Vector4f(0.05f, 0.2f, 0.2f, 0.5f), new Vector4f(0.3f), 7, 2);
		//this.getGUI().add(gridPanel);
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(1, 0, 0 , 1), new Vector2f(10, 10)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 1, 0 , 1)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 0, 1 , 1), new Vector2f(0, 10)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 1, 1 , 1)));
		
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 1, 1 , 1)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 0, 1 , 1), new Vector2f(10, 0)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 1, 0 , 1)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(1, 0, 0 , 1)));
		
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(1, 0, 0 , 1), new Vector2f(10, 10)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 1, 0 , 1)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 0, 1 , 1), new Vector2f(0, 10)));
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 1, 1 , 1)));
		
		Label lbl = new Label(new Vector2f(0.2f), "TESTE");
		gridPanel.addCell(lbl);
		gridPanel.addCell(new Panel(new Vector4f(), new Vector4f(0, 0, 1 , 1), new Vector2f(10, 0)));

		slider = new Slider(0, -10, 10);
		slider.setBounds(new Vector4f(0.4f, 0.85f, 0.2f, 0.015f));
		//this.getGUI().add(slider);
		
		superPanel = new GameObjectPanel();
		superPanel.setBounds(new Vector4f(0.73f, 0.1f, 0.25f, 0.7f));
		this.getGUI().add(superPanel);
		
		line = new GameObject();
		scene.addGameObject(line);

	}
	
	float test = 0;
	Vector3f origin = new Vector3f();
	Vector3f destination = new Vector3f();
	Vector2f mousePos = new Vector2f();
	Vector3f camPos = new Vector3f();
	RayAabIntersection ray = new RayAabIntersection();
	BoundingBox selected;
	boolean drag = false;
	Vector3f planePoint;
	
	@Override
	protected void update() {
		
		super.update();
		
		//cube.getTransform().getPosition().x = slider.getValue();
//		Vector3f ndcCoords = this.getScene().getCamera().getViewProjection()
//				.transformProject(worldPosition, new Vector3f());
		mousePos.x = getInput().getMousePosition().x;
		mousePos.y = this.getViewPort()[3] - getInput().getMousePosition().y;
		
		FrustumIntersection frustumInt = new FrustumIntersection();
		frustumInt.set(camera.getViewProjection());
		
		this.getScene().getCamera().getViewProjection().unprojectRay(mousePos, this.getViewPort(), origin, destination);
		camPos.set(getScene().getCamera().getTransform().getTransformedPosition()); 
		
		if (drag) {
			
			Vector3f planeNormal = new Vector3f(this.getScene().getCamera().getTransform().getForward());
			//planeNormal.mul(-1);
			//System.out.println(planeNormal);
			
			float distance = planePoint.distance(camPos);
			
			float frustumHeight = (float) (2.0f * distance * Math.tan( Math.toRadians(70f * 0.5f)));
			
			//System.out.println(frustumHeight);
			
			float dotProduct = planeNormal.dot(planePoint);
			
			Vector3f planeA = new Vector3f(1, 0, planePoint.z);
			Vector3f planeB = new Vector3f(0, 1, planePoint.z);
			Vector3f planeC = new Vector3f(1, 1, planePoint.z);
//			System.out.println(planeA);
//			System.out.println(planeB);
//			System.out.println(planeC);
//			System.out.println("\n\n");
			Vector3f to = MyMath.intersectionPointLinePlane(origin, destination, planeA, planeB, planeC);
			selected.getParent().getTransform().setPosition(to);
			selected.transform(selected.getParent().getTransform().getTransformation());
			System.out.println(to.toString(NumberFormat.getInstance()));
			
		}
		
		//System.out.println("Origin: " + origin + "\t Destination: " + destination);
		
		//float hit = Intersectionf.intersectRayTriangle(origin, destination, , v1, v2, 1E-6f);
		
		
//		camPos.y -= 0.01f;
//		//System.out.println("CAMERA AT: " + camPos.x + ", " + camPos.y + ", " + camPos.z);
		//Vertex[] vertices = new Vertex[2];
		//vertices[1] = new Vertex(camPos);
		//vertices[0] = new Vertex(new Vector3f(0));
		
		//int[] indices = {0, 1};
		
		
//		line.getComponents().clear();
//		line.addGameComponent(new Line3D(origin, destination));
		
		ray.set(origin.x, origin.y, origin.z, destination.x, destination.y, destination.z);
		
		Utils.sort(boxes, camPos);
		
		boolean hitted = false;
		if (!drag)
		for (int i = 0; i < boxes.size(); i++) {

			BoundingBox b = boxes.get(i);
			
			//b.transform(b.getParent().getTransform().getTransformation());
			
			//min.mul(b.getParent().getTransform().getTransformation());
			//max.mul(b.getParent().getTransform().getTransformation());
			//b.getParent().getTransform().getTransformation().transform(min);
			//b.getParent().getTransform().getTransformation().transform(max);
			
//			System.out.println("MIN: " + min);
//			System.out.println("MAX: " + max);
			
			if (ray.test(b.minX, b.minY, b.minZ, b.maxX, b.maxY, b.maxZ) && !hitted) {
				b.setColor(GS.COLOR.RED);
				hitted = true;
				
				if (this.getInput().getMouseLeftButton()) {
					selected = b;
					drag = true;
					planePoint = new Vector3f(selected.getParent().getTransform().getTransformedPosition());
				} 
				
			} else {
				b.setColor(GS.COLOR.GREEN);
			}
		}
		
		
		if (!this.getInput().getMouseLeftButton()) {
			drag = false;
			selected = null;
		}
		
	}
	
	
	@Override
	protected void input() {
		super.input();
		
		
	}

}
