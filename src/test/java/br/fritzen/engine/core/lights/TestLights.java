package br.fritzen.engine.core.lights;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameComponent;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.Attenuation;
import br.fritzen.engine.core.gameobject.lights.PointLight;
import br.fritzen.engine.core.gameobject.lights.SpotLight;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.rendering.NormalMapRenderer;
import br.fritzen.engine.utils.MyMath;

public class TestLights extends Game {

	public TestLights() {
		super(1280, 720, "Lights Test");
		
	}

	public static void main(String[] args) {
		new TestLights().start();
	}

	@Override
	protected void init() {
		
		this.setShadowMap(true);
		
		Scene scene = Game.getScene();
		
		scene.setAmbientLight(new Vector3f(0.2f));
		
		SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1f, new Vector3f(1f, -1f, -0.5f));
		scene.setSunLight(sun);
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1280f/720f, 0.1f, 1000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 5));
		camera.getTransform().getRotation().rotate((float)Math.toRadians(-30), 0, 0);
		camera.addGameComponent(new FreeMove(this.getInput(), 0.05f));
		camera.addGameComponent(new FreeLook(this.getInput(), GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
	
		GameObject plane = new GameObject();
		
		//LOAD SIMPLE TEXTURE
		Texture initialTexture = new Texture("src/main/resources/images/wood-floor2.jpg");
		
		//CREATE NORMAL MAP TEXTURE FROM A TEXTURE 
		Texture normalTexture = new NormalMapRenderer(1024, 10).render(initialTexture).getNormalMap();;
		plane.addGameComponent(
				new MeshRenderer(
						new Mesh("src/main/resources/models/plane/plane.obj"),
						new Material()
							.setTexture(initialTexture)
							.setNormalMap(normalTexture)
							.setSpecularExponent(64f)));
		
		plane.getTransform().setScale(new Vector3f(20));
		scene.addGameObject(plane);
		
		
		GameObject monkey = new GameObject();
		monkey.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/monkey/model.obj")));
		monkey.getTransform().setPosition(new Vector3f(0, 0, -5));
		
		scene.addGameObject(monkey);
		camera.addChild(monkey);
		
		//POINT LIGHTS
		GameObject pointLight1 = new GameObject("Point Light 001");
		GameComponent lightComp1 = new PointLight(GS.X_AXIS, 0.5f, new Attenuation());
		pointLight1.addGameComponent(lightComp1);
		pointLight1.getTransform().setPosition(new Vector3f(-5, 0.2f, 5));
		scene.addGameObject(pointLight1);
		
		GameObject pointLight2 = new GameObject("Point Light 002");
		GameComponent lightComp2 = new PointLight(GS.Y_AXIS, 0.5f, new Attenuation());
		pointLight2.addGameComponent(lightComp2);
		pointLight2.getTransform().setPosition(new Vector3f(0, 0.2f, 5));
		scene.addGameObject(pointLight2);
		
		GameObject pointLight3 = new GameObject("Point Light 003");
		GameComponent lightComp3 = new PointLight(GS.Z_AXIS, 0.5f, new Attenuation());
		pointLight3.addGameComponent(lightComp3);
		pointLight3.getTransform().setPosition(new Vector3f(5, 0.2f, 5));
		scene.addGameObject(pointLight3);
		
		
		//SPOT LIGHTS
		GameObject spotLight1 = new GameObject();
		GameComponent spotComp1 = new SpotLight(GS.X_AXIS, 2f, new Attenuation(), 0.2f);
		spotLight1.addGameComponent(spotComp1);
		spotLight1.getTransform().setPosition(new Vector3f(-15, 5, -10));
		spotLight1.getTransform().rotate(GS.X_AXIS, -MyMath.toRadians(90f));
		scene.addGameObject(spotLight1);
		
		GameObject spotLight2 = new GameObject();
		GameComponent spotComp2 = new SpotLight(GS.Y_AXIS, 2f, new Attenuation(), 0.2f);
		spotLight2.addGameComponent(spotComp2);
		spotLight2.getTransform().setPosition(new Vector3f(0, 5, -10));
		spotLight2.getTransform().rotate(GS.X_AXIS, -MyMath.toRadians(90f));
		scene.addGameObject(spotLight2);
		
		GameObject spotLight3 = new GameObject();
		GameComponent spotComp3 = new SpotLight(GS.Z_AXIS, 3f, new Attenuation(), 0.2f);
		spotLight3.addGameComponent(spotComp3);
		spotLight3.getTransform().setPosition(new Vector3f(15, 5, -10));
		spotLight3.getTransform().rotate(GS.X_AXIS, -MyMath.toRadians(90f));
		scene.addGameObject(spotLight3);
		

	}

	
	
}
