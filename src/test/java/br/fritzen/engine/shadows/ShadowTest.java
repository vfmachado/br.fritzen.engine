package br.fritzen.engine.shadows;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.GS;
import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.NormalMappingTest;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.Attenuation;
import br.fritzen.engine.core.gameobject.lights.SpotLight;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.material.Material;
import br.fritzen.engine.core.gameobject.material.Texture;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.utils.MyMath;

public class ShadowTest  extends Game {

	GameObject floor;
	GameObject cube;
	SunLight sun;
	GameObject sphere;
	
	public static void main(String[] args) {
		
		ShadowTest program = new ShadowTest(1280, 720, "Shadow Test");
		program.start();
		
	}

	
	public ShadowTest(int width, int height, String title) {
		super(width, height, title);
		
	}

	
	@Override
	protected void init() {

		this.setShadowMap(true); //DEFAULT = TRUE
		
		Scene scene = Game.getScene();
		
		this.setAmbientColor(new Vector4f(0.4f));
		
		scene.setAmbientLight(new Vector3f(0.2f));
		sun = new SunLight(new Vector3f(1, 1, 1), 2, new Vector3f(0.5f, 1f, 1f));
		scene.setSunLight(sun);
		
		
		Camera camera = new Camera((float) Math.toRadians(70f), 1280f/720f, 0.1f, 500.0f);
		camera.addGameComponent(new FreeMove(this.getInput(), 0.25f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.5f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		camera.getTransform().setPosition(new Vector3f(2.5f, 5, 15));
		scene.setCamera(camera);
	
		sphere = new GameObject("sun");
		Material sunMat = new Material();
		sunMat.setAmbientColor(new Vector3f(255, 255, 0));
		sunMat.setDiffuseColor(new Vector3f(255, 255, 0));
		sphere.addGameComponent(new MeshRenderer(new Mesh("src/main/resources/models/sphere/model.obj"), sunMat));
		sphere.getTransform().setPosition(new Vector3f(-50, 100, 100));
		sphere.castShadow(false);
		sphere.getTransform().setScale(new Vector3f(50));
		
		scene.addGameObject(sphere);
		
		
		//TEST 1
		Mesh mesh1 = new Mesh("src/main/resources/models/plane/plane.obj");
		Material material1 = new Material();
		//material1.setDiffuseColor(new Vector3f(1));
		material1.setTexture(new Texture("src/main/resources/images/defaultTexture.png"));
		material1.setSpecularExponent(1);
		floor = new GameObject("obj1");
		floor.addGameComponent(new MeshRenderer(mesh1, material1));
		scene.addGameObject(floor);
		
		floor.getTransform().setScale(new Vector3f(50f, 1f, 50f));

		//TEST 2
		Mesh mesh2 = new Mesh("src/main/resources/models/dragon/model.obj");
		Material material2 = new Material();
		material2.setSpecularExponent(64);
		
		cube = new GameObject("obj2");
		cube.addGameComponent(new MeshRenderer(mesh2, material2));
		cube.getTransform().getPosition().y += 20;
		cube.getTransform().setScale(new Vector3f(1));
		scene.addGameObject(cube);
		
		cube = new GameObject("obj2");
		cube.addGameComponent(new MeshRenderer(mesh2, material2));
		cube.getTransform().getPosition().x += 20;
		cube.getTransform().getPosition().y += 5;
		cube.getTransform().setScale(new Vector3f(1));
		scene.addGameObject(cube);
		
		cube = new GameObject("obj2");
		cube.addGameComponent(new MeshRenderer(mesh2, material2));
		cube.getTransform().getPosition().z += -25;
		cube.getTransform().getPosition().x += -25;
		cube.getTransform().setScale(new Vector3f(0.5f));
		scene.addGameObject(cube);
		
		cube = new GameObject("obj2");
		cube.addGameComponent(new MeshRenderer(mesh2, material2));
		cube.getTransform().getPosition().x -= 14;
		cube.getTransform().getPosition().z = 14;
		cube.getTransform().getPosition().y = 10;
		cube.getTransform().setScale(new Vector3f(0.4f));
		scene.addGameObject(cube);
		
	}
	
	float temp = 0;
	Vector3f camPos = new Vector3f();
	@Override
	protected void update() {
		super.update();
		
//		temp += 0.2f;
//		sun.getDirection().x = (float) Math.cos(MyMath.toRadians(temp));
//		sun.getDirection().z = (float) Math.sin(MyMath.toRadians(temp));
	
		camPos = Game.getScene().getCamera().getTransform().getPosition();
		sphere.getTransform().getPosition().x = camPos.x - 50;
		sphere.getTransform().getPosition().y = camPos.y + 100;
		sphere.getTransform().getPosition().z = camPos.z + 100;
		
		
	}

	
	@Override
	protected void input() {
		super.input();
	}
}


