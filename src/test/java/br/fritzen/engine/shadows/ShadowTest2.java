package br.fritzen.engine.shadows;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import br.fritzen.engine.core.Game;
import br.fritzen.engine.core.gameobject.GameObject;
import br.fritzen.engine.core.gameobject.Scene;
import br.fritzen.engine.core.gameobject.components.FreeLook;
import br.fritzen.engine.core.gameobject.components.FreeMove;
import br.fritzen.engine.core.gameobject.components.MeshRenderer;
import br.fritzen.engine.core.gameobject.lights.SunLight;
import br.fritzen.engine.core.gameobject.sceneobjects.Camera;
import br.fritzen.engine.core.gameobject.utils.BoundingBox;
import br.fritzen.engine.core.gameobject.utils.Mesh;
import br.fritzen.engine.utils.MyMath;


public class ShadowTest2  extends Game {

	private GameObject cube;
	private Mesh cubeMesh;
	private ArrayList<BoundingBox> boxes;
	private Camera camera; 
	
	
	public static void main(String[] args) {

		ShadowTest2 program = new ShadowTest2(1280, 720, "Shadow Test 2");
		program.start();
		
	}

	
	public ShadowTest2(int width, int height, String title) {
		super(width, height, title);	
	}
	
	
	@Override
	protected void init() {
		
		this.setShadowMap(true);

		Scene scene = Game.getScene();
		
		this.setAmbientColor(new Vector4f(0.1f));
		
		scene.setAmbientLight(new Vector3f(0.4f));
		SunLight sun = new SunLight(new Vector3f(1, 1, 1), 1, new Vector3f(0.5f, 1f, 1f));
		scene.setSunLight(sun);
		
		camera = new Camera((float) Math.toRadians(70f), 1280/720f, 0.01f, 5000.0f);
		camera.getTransform().setPosition(new Vector3f(0, 5, 10));
		//camera.getTransform().getRotation().rotate(MyMath.toRadians(-30), 0, 0);
		
		camera.addGameComponent(new FreeMove(this.getInput(), 2f));
		camera.addGameComponent(new FreeLook(this.getInput(), 0.5f, GLFW.GLFW_MOUSE_BUTTON_RIGHT));
		
		scene.setCamera(camera);
		
		
		//TEST 1
		Mesh mesh1 = new Mesh("src/main/resources/models/plane/plane.obj");
		
		GameObject floor = new GameObject("obj1");
		floor.addGameComponent(new MeshRenderer(mesh1));
		floor.getTransform().setScale(new Vector3f(1000, 1, 1000));
		scene.addGameObject(floor);
		
		boxes = new ArrayList<BoundingBox>();
		cubeMesh = new Mesh("src/main/resources/models/cube/model.obj");
		
		int dist = 1000;
		for (int i = 0; i < 1_000; i++) {
			cube = new GameObject("cube");
			boxes.add(cubeMesh.getBoundingBox());
			cube.addGameComponent(new MeshRenderer(cubeMesh));
			cube.addGameComponent(boxes.get(i));
			scene.addGameObject(cube);
			cube.getTransform().getRotation().rotateAxis((float)Math.toRadians(new Random().nextInt(366)), new Vector3f(new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat()));
			cube.getTransform().setPosition(new Vector3f(new Random().nextFloat() * dist - dist/2, new Random().nextFloat() * 50, new Random().nextFloat() * dist - dist/2));
			cube.getTransform().setScale(new Vector3f(new Random().nextFloat() * 5, new Random().nextFloat() * 5, new Random().nextFloat() * 5));
			boxes.get(i).getParent().getTransform().preProcess();
			boxes.get(i).transform(boxes.get(i).getParent().getTransform().getTransformation());
			
		}
		

	}
	
	
	@Override
	protected void update() {	
		super.update();
	}
	
	
	@Override
	protected void input() {
		super.input();
	}

}
